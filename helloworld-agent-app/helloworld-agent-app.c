/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include "helloworld-agent-app.h"
#include "org.apertis.HelloWorldAgentApp.Agent.TickBoard.h"

#include <mildenhall/mildenhall.h>

/* Agent Bus ID */
#define HLW_AGENT_ID "org.apertis.HelloWorldAgentApp.Agent"

/**
 * HlwAgentApp:
 *
 * Object used to hold UI information
 *
 */
struct _HlwAgentApp
{
  GApplication parent;

  ClutterActor *stage;                  /* owned */

  MildenhallButtonSpeller *button;      /* unowned */

  HlwDBusTickBoard *hlw_tick_board;     /* owned */
  guint hlw_tick_board_bus_id;
};

G_DEFINE_TYPE (HlwAgentApp, hlw_agent_app, G_TYPE_APPLICATION);

enum
{
  COLUMN_TEXT = 0,
  COLUMN_LAST = COLUMN_TEXT,
  N_COLUMN,
};

static void
on_current_tick_updated (GObject *object,
                         GParamSpec *psec,
                         gpointer user_data)
{
  int tick = 0;
  g_autofree gchar *tick_text = NULL;
  HlwAgentApp *self = HLW_AGENT_APP (user_data);

  /* Draw tick value on the screen */
  g_object_get (self->hlw_tick_board, "current-tick", &tick, NULL);
  tick_text = g_strdup_printf ("Tick Value : %d", tick);
  v_button_speller_set_text (CLUTTER_ACTOR (self->button), tick_text);
}

static void
on_toggle_tick_callback (GObject *source_object,
                         GAsyncResult *result,
                         gpointer user_data)
{
  g_autoptr (GError) error = NULL;
  HlwDBusTickBoard *tick_board = HLW_DBUS_TICK_BOARD (source_object);

  if (!hlw_dbus_tick_board_call_toggle_tick_finish (tick_board, result, &error))
    {
      g_warning ("Failed to finish toggle_tick request (reason: %s)",
        error->message);
    }
}

static void
button_pressed_cb (MildenhallButtonSpeller *button,
                   gpointer user_data)
{
  HlwAgentApp *self = HLW_AGENT_APP (user_data);

  if (self->hlw_tick_board == NULL)
    {
      g_message ("Agent proxy isn't ready.");

      return;
    }

  g_message ("Clicked Toggle button");

  hlw_dbus_tick_board_call_toggle_tick (self->hlw_tick_board,
                                        NULL,
                                        on_toggle_tick_callback,
                                        self);
}

static void
hlw_dbus_tick_board_ready_callback (GObject *source_object,
                                    GAsyncResult *res,
                                    gpointer user_data)
{
  g_autoptr (GError) error = NULL;
  HlwAgentApp *self = HLW_AGENT_APP (user_data);

  self->hlw_tick_board = hlw_dbus_tick_board_proxy_new_finish (res, &error);

  g_signal_connect (self->hlw_tick_board,
      "notify::current-tick", G_CALLBACK (on_current_tick_updated),
      self);
 
  g_message ("Created TickBoard Proxy (%p).", self->hlw_tick_board);
}

static void
bus_name_appeared (GDBusConnection *connection,
                   const gchar *name,
                   const gchar *name_owner,
                   gpointer user_data)
{
  HlwAgentApp *self = HLW_AGENT_APP (user_data);

  g_message ("Bus name %s now owned by %s", name, name_owner);

  if (g_strcmp0 (name, "org.apertis.HelloWorldAgentApp.Agent") == 0)
    {
      /* The TickBoard will be created asynchronously. */
      hlw_dbus_tick_board_proxy_new (connection,
        G_DBUS_PROXY_FLAGS_NONE,
        "org.apertis.HelloWorldAgentApp.Agent",
        "/org/apertis/HelloWorldAgentApp/Agent/TickBoard",
        NULL,
        hlw_dbus_tick_board_ready_callback, self);
    }  
}

static void
bus_name_vanished (GDBusConnection *connection,
                   const gchar *name,
                   gpointer user_data)
{
  HlwAgentApp *self = HLW_AGENT_APP (user_data);

  if (g_strcmp0 (name, "org.apertis.HelloWorldAgentApp.Agent") == 0)
    {
      g_message ("Destroying TickBoard proxy (%p).", self->hlw_tick_board);
      g_clear_object (&self->hlw_tick_board);
    }
}

static gboolean
on_stage_destroy (ClutterActor *stage,
                  gpointer user_data)
{
  GApplication *app = user_data;

  g_debug ("The main stage is destroyed.");

  /* chain up */
  CLUTTER_ACTOR_GET_CLASS (stage)->destroy (stage);

  /* The main window is detroyed (closed) so the application
   * needs to be released */
  g_application_release (app);

  return TRUE;
}

static void
startup (GApplication *app)
{
  g_autoptr (GObject) widget_object = NULL;
  g_autoptr (ThornburyItemFactory) item_factory = NULL;
  g_autofree gchar *widget_properties_file = NULL;
  ThornburyModel *model;

  HlwAgentApp *self = HLW_AGENT_APP (app);

  g_application_hold (app);

  /* chain up */
  G_APPLICATION_CLASS (hlw_agent_app_parent_class)->startup (app);

  /* Start watching Agent on the bus */
  self->hlw_tick_board_bus_id = g_bus_watch_name (
      G_BUS_TYPE_SESSION, HLW_AGENT_ID,
      G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
      bus_name_appeared,
      bus_name_vanished,
      self, NULL);

  /* build ui components */
  self->stage = mildenhall_stage_new (g_application_get_application_id (app));

  g_signal_connect (G_OBJECT (self->stage),
      "destroy", G_CALLBACK (on_stage_destroy),
      self);

  widget_properties_file = g_build_filename (PKGDATADIR,
                                             "button_speller_text_prop.json",
                                             NULL);

  item_factory = thornbury_item_factory_generate_widget_with_props (
      MILDENHALL_TYPE_BUTTON_SPELLER,
      widget_properties_file);

  g_object_get (item_factory,
                "object", &widget_object,
                NULL);

  self->button = MILDENHALL_BUTTON_SPELLER (widget_object);

  model = THORNBURY_MODEL (
      thornbury_list_model_new (N_COLUMN,
                                G_TYPE_STRING,
                                NULL,
                                -1));

  thornbury_model_append (model, 
                          COLUMN_TEXT, "Click!",
                          -1);

  g_object_set (self->button,
                "model", model,
                NULL);

  g_clear_object (&model);

  clutter_actor_add_child (self->stage, CLUTTER_ACTOR (self->button));

  g_signal_connect (self->button,
      "button-press", G_CALLBACK (button_pressed_cb),
      self);
}

static void
activate (GApplication *app)
{
  HlwAgentApp *self = HLW_AGENT_APP (app);

  clutter_actor_show (self->stage);
}

static void
hlw_agent_app_dispose (GObject *object)
{
  HlwAgentApp *self = HLW_AGENT_APP (object);

  if (self->hlw_tick_board_bus_id != 0)
    {
      g_bus_unwatch_name (self->hlw_tick_board_bus_id);
      self->hlw_tick_board_bus_id = 0;
    }

  g_clear_object (&self->hlw_tick_board);

  g_clear_object (&self->stage);

  G_OBJECT_CLASS (hlw_agent_app_parent_class)->dispose (object);
}

static void
hlw_agent_app_class_init (HlwAgentAppClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GApplicationClass *app_class = G_APPLICATION_CLASS (klass);

  object_class->dispose = hlw_agent_app_dispose;

  app_class->startup = startup;
  app_class->activate = activate;
}

static void
hlw_agent_app_init (HlwAgentApp *self)
{
}
