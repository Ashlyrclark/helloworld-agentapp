HelloWorld Agent Application
=============================

This is a sample repository to demonstrate how to interact between an application
bundle and its own agent for Apertis.

Building from git
=================

$ sudo apt install git build-essential automake libtool pkg-config\
	libglib2.0-dev libclutter-1.0-dev gobject-introspection gnome-common \
	hotdoc ribchester-tools policykit-1
$ ./autogen.sh --prefix=/Applications/org.apertis.HelloWorldAgentApp
$ make
$ make install DESTDIR=$(pwd)/DESTDIR
$ sudo ribchester-bundle install org.apertis.HelloWorldAgentApp 0.1.0 `pwd`/DESTDIR

Building flatpak package
========================

$ APERTIS_RELEASE=v2021pre
$ flatpak install org.apertis.mildenhall.Platform//${APERTIS_RELEASE} org.apertis.mildenhall.Sdk//${APERTIS_RELEASE}
$ mkdir -p `pwd`/repo
$ flatpak-builder --repo=repo --default-branch=${APERTIS_RELEASE} --force-clean build-dir flatpak-recipe.yaml

Licensing
=========

This sample application is licensed under the MPLv2; see COPYING for more details.

Bugs
====

Bug reports and (git formatted) patches should be submitted to the Phabricator
instance for Apertis:

http://phabricator.apertis.org/

Contact
=======

See AUTHORS.
