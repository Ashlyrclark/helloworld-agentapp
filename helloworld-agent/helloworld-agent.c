/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "helloworld-agent.h"
#include "helloworld-tickboard.h"

/**
 * HlwAgent:
 *
 * Management object which possesses actual D-Bus objects.
 */
struct _HlwAgent
{
  GApplication parent;

  HlwTickBoard *tick_board;             /* owned */
};

G_DEFINE_TYPE (HlwAgent, hlw_agent, G_TYPE_APPLICATION)

static void
hlw_agent_activate (GApplication *app)
{
  g_message ("Agent is activated.");
}

static gboolean
hlw_agent_dbus_register (GApplication    *app,
                         GDBusConnection *connection,
                         const gchar     *object_path,
                         GError         **error)
{
  gboolean ret;
  HlwAgent *self = HLW_AGENT (app);

  g_application_hold (app);

  g_message ("Registered D-Bus (path: %s)", object_path);

  /* chain up */
  ret = G_APPLICATION_CLASS (hlw_agent_parent_class)->dbus_register (
      app,
      connection,
      object_path,
      error);

  if (ret &&
      !g_dbus_interface_skeleton_export (
          G_DBUS_INTERFACE_SKELETON (self->tick_board),
          connection,
          "/org/apertis/HelloWorldAgentApp/Agent/TickBoard",
          error))
    {
      g_warning ("Failed to export TickBoard D-Bus interface (reason: %s)",
          (*error)->message);
    }

  return ret;
}

static void
hlw_agent_dbus_unregister (GApplication    *app,
                           GDBusConnection *connection,
                           const gchar     *object_path)
{
  HlwAgent *self = HLW_AGENT (app);

  if (self->tick_board)
    g_dbus_interface_skeleton_unexport (G_DBUS_INTERFACE_SKELETON (self->tick_board));

  g_application_release (app);

  g_message ("Unregistered D-Bus (path: %s)", object_path);

  /* chain up */
  G_APPLICATION_CLASS (hlw_agent_parent_class)->dbus_unregister (
      app,
      connection,
      object_path);
}

static void
hlw_agent_dispose (GObject *object)
{
  HlwAgent *self = HLW_AGENT (object);

  g_clear_object (&self->tick_board);

  G_OBJECT_CLASS (hlw_agent_parent_class)->dispose (object);
}

static void
hlw_agent_class_init (HlwAgentClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GApplicationClass *app_class = G_APPLICATION_CLASS (klass);

  object_class->dispose = hlw_agent_dispose;

  app_class->activate = hlw_agent_activate;
  app_class->dbus_register = hlw_agent_dbus_register;
  app_class->dbus_unregister = hlw_agent_dbus_unregister;
}

static void
hlw_agent_init (HlwAgent *self)
{
  self->tick_board = hlw_tick_board_new ();
}
